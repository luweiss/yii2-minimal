<?php
/**
 * @copyright ©2018 Lu Wei
 * @author Lu Wei
 * @link http://www.luweiss.com/
 *
 * Created by IntelliJ IDEA.
 * Date Time: 2018/3/23 10:54
 */
$db = [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;port=3306;dbname=db',
    'username' => 'db',
    'password' => 'db',
    'charset' => 'utf8',
    'tablePrefix' => 'tbl_',
];
return $db;