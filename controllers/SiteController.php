<?php
/**
 * @copyright ©2018 Lu Wei
 * @author Lu Wei
 * @link http://www.luweiss.com/
 *
 * Created by IntelliJ IDEA.
 * Date Time: 2018/3/23 10:16
 */


namespace app\controllers;


use app\models\SiteIndexForm;

class SiteController extends Controller
{
    public function actionIndex()
    {
        $form = new SiteIndexForm();
        return $this->render('index', [
            'title' => $form->getTitle(),
        ]);
    }
}