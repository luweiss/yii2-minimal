<?php
/**
 * @copyright ©2018 Lu Wei
 * @author Lu Wei
 * @link http://www.luweiss.com/
 *
 * Created by IntelliJ IDEA.
 * Date Time: 2018/3/23 10:17
 */


namespace app\models;


class SiteIndexForm extends Model
{
    public function getTitle()
    {
        return 'Yii2 Minimal';
    }
}